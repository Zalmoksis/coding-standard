<?xml version="1.0"?>
<ruleset name="Zalmoksis">
    <description>Zalmoksis coding standard.</description>

    <!-- Arrays -->
    <!-- Arrays must be declared using the short syntax -->
    <rule ref="Generic.Arrays.DisallowLongArraySyntax"/>

    <!-- Classes -->
    <!-- The opening brace of a class must be on the same line as the class declaration -->
    <rule ref="Generic.Classes.OpeningBraceSameLine"/>
    <!-- Classes must be declared one per file -->
    <!-- and with at least a one level namespace -->
    <rule ref="PSR1.Classes.ClassDeclaration"/>
    <!-- Properties have to be declared with a visibility keyword -->
    <rule ref="PSR2.Classes.PropertyDeclaration"/>
    <!-- Classes have to be instantiated with parentheses -->
    <rule ref="PSR12.Classes.ClassInstantiation"/>
    <!-- Classes must not reference themselves by name, -->
    <!-- but using the self keyword instead -->
    <rule ref="Squiz.Classes.SelfMemberReference"/>
    <!-- Class name must be in upper camel case -->
    <rule ref="Squiz.Classes.ValidClassName"/>

    <!-- Control structures -->
    <!-- Inline control structures are not allowed -->
    <rule ref="Generic.ControlStructures.InlineControlStructure"/>
    <!-- Control structures must follow PEAR signature -->
    <rule ref="PEAR.ControlStructures.ControlSignature"/>
    <!-- Multiline control structures must follow PEAR standard -->
    <rule ref="PEAR.ControlStructures.MultiLineCondition"/>
    <!-- There must be one space between each condition of a foreach loops -->
    <rule ref="Squiz.ControlStructures.ForEachLoopDeclaration"/>
    <!-- Already covered by Generic.PHP.LowerCaseKeyword -->
    <rule ref="Squiz.ControlStructures.ForEachLoopDeclaration.AsNotLower">
        <severity>0</severity>
    </rule>

    <!-- Files -->
    <!-- Files must not use byte order mark -->
    <rule ref="Generic.Files.ByteOrderMark"/>
    <rule ref="Generic.Files.InlineHTML"/>
    <!-- A new line must be represented using a single line feed character -->
    <rule ref="Generic.Files.LineEndings">
        <properties>
            <property name="eolChar" value="\n"/>
        </properties>
    </rule>
    <!-- The line length must be limited to 120 characters -->
    <rule ref="Generic.Files.LineLength">
        <properties>
            <property name="lineLimit" value="120"/>
            <property name="absoluteLineLimit" value="120"/>
        </properties>
    </rule>
    <!-- There must be one object structure per file -->
    <!-- Already included in PSR1.Classes.ClassDeclaration -->
    <!-- <rule ref="Generic.Files.OneObjectStructurePerFile"/> -->
    <rule ref="PEAR.Files.IncludingFile"/>
    <!-- A file should not declare new symbols and casue side effects -->
    <rule ref="PSR1.Files.SideEffects"/>
    <!-- Files must not end with a PHP closing tag -->
    <rule ref="PSR2.Files.ClosingTag"/>
    <!-- Files must end with single newline -->
    <rule ref="PSR2.Files.EndFileNewline"/>

    <!-- Functions -->
    <rule ref="Generic.Functions.FunctionCallArgumentSpacing"/>
    <rule ref="Generic.Functions.FunctionCallArgumentSpacing.TooMuchSpaceAfterComma">
        <severity>0</severity>
    </rule>
    <!-- Function opening brace -->
    <!-- must be in the same line as the function declaration -->
    <rule ref="Generic.Functions.OpeningFunctionBraceKernighanRitchie"/>
    <!-- Function parameters with default values -->
    <!-- must go at the end of the declaration -->
    <rule ref="PEAR.Functions.ValidDefaultValue"/>
    <!-- Squiz spacing rules -->
    <!-- Possible duplicate with Generic.Functions.FunctionCallArgumentSpacing -->
    <rule ref="Squiz.Functions.FunctionDeclarationArgumentSpacing">
        <properties>
            <property name="equalsSpacing" value="1"/>
        </properties>
    </rule>
    <rule ref="Squiz.Functions.FunctionDeclarationArgumentSpacing.SpacingAfterHint">
        <severity>0</severity>
    </rule>
    <rule ref="Squiz.Functions.FunctionDeclarationArgumentSpacing.SpacingBeforeArg">
        <severity>0</severity>
    </rule>
    <!-- There must be no global functions declared -->
    <rule ref="Squiz.Functions.GlobalFunction"/>

    <!-- Formatting -->
    <rule ref="Generic.Formatting.DisallowMultipleStatements"/>

    <!-- Keywords -->
    <rule ref="PSR12.Keywords.ShortFormTypeKeywords"/>

    <!-- Methods -->
    <!-- Method name must be in lower camel case -->
    <rule ref="PSR1.Methods.CamelCapsMethodName"/>

    <!-- Metrics -->
    <rule ref="Generic.Metrics.CyclomaticComplexity">
        <properties>
            <property name="complexity" value="5"/>
            <property name="absoluteComplexity" value="5"/>
        </properties>
    </rule>
    <rule ref="Generic.Metrics.NestingLevel">
        <properties>
            <property name="nestingLevel" value="3"/>
            <property name="absoluteNestingLevel" value="3"/>
        </properties>
    </rule>

    <!-- Namespaces -->
    <!-- There must to be one blank line after the namespace declaration -->
    <rule ref="PSR2.Namespaces.NamespaceDeclaration"/>

    <!-- Naming conventions -->
    <rule ref="Generic.NamingConventions.CamelCapsFunctionName"/>
    <rule ref="Generic.NamingConventions.UpperCaseConstantName"/>

    <!-- Scope -->
    <rule ref="Squiz.Scope.MemberVarScope"/>
    <!-- $this must not be used in a static context -->
    <rule ref="Squiz.Scope.StaticThisUsage"/>

    <!-- Operators -->
    <!-- Binary and ternary operators have to be surrounded spaces -->
    <rule ref="PSR12.Operators.OperatorSpacing"/>

    <!-- PHP -->
    <rule ref="Generic.PHP.CharacterBeforePHPOpeningTag"/>
    <rule ref="Generic.PHP.DisallowAlternativePHPTags"/>
    <rule ref="Generic.PHP.DisallowShortOpenTag"/>
    <rule ref="Generic.PHP.DisallowShortOpenTag.EchoFound">
        <severity>0</severity>
    </rule>
    <rule ref="Generic.PHP.ForbiddenFunctions">
        <properties>
            <property name="forbiddenFunctions" type="array">
                <element key="is_null" value="null"/>
            </property>
        </properties>
    </rule>
    <!-- Following PHP constants must be lowercase: true, false, null -->
    <rule ref="Generic.PHP.LowerCaseConstant"/>
    <!-- All PHP keywords must be lowercase -->
    <rule ref="Generic.PHP.LowerCaseKeyword"/>
    <!-- All PHP types must be lowercase -->
    <rule ref="Generic.PHP.LowerCaseType"/>
    <!-- The error silencing operator `@` must not be used -->
    <rule ref="Generic.PHP.NoSilencedErrors"/>
    <!-- There must be no syntax errors -->
    <rule ref="Generic.PHP.Syntax"/>
    <!-- The following functions must not be used: -->
    <!-- # error_log, print_r, var_dump -->
    <rule ref="Squiz.PHP.DiscouragedFunctions">
        <properties>
            <property name="error" value="true"/>
        </properties>
    </rule>
    <rule ref="Squiz.PHP.Eval"/>
    <!-- The `global` keyword must not be used -->
    <rule ref="Squiz.PHP.GlobalKeyword"/>
    <!-- The PHP functions must be lowercase -->
    <rule ref="Squiz.PHP.LowercasePHPFunctions"/>
    <!-- There must be no non-executable code -->
    <rule ref="Squiz.PHP.NonExecutableCode"/>

    <!-- White space -->
    <rule ref="Generic.WhiteSpace.DisallowTabIndent"/>
    <rule ref="Generic.WhiteSpace.ScopeIndent"/>
    <rule ref="PEAR.WhiteSpace.ObjectOperatorIndent"/>
    <!-- There must be a single space between following language constructs -->
    <!-- and their content: -->
    <!-- # `echo`, `print` -->
    <!-- # `return` -->
    <!-- # `include`, `include_once`, `require`, `require_once` -->
    <!-- # `new` -->
    <rule ref="Squiz.WhiteSpace.LanguageConstructSpacing"/>
    <!-- <rule ref="PEAR.WhiteSpace.ScopeClosingBrace"/> -->
    <!-- <rule ref="PEAR.WhiteSpace.ScopeIndent"/> -->
    <!-- ... -->
    <!-- <rule ref="Squiz.WhiteSpace.ScopeClosingBrace"/> -->
    <!-- There must be exactly one space after scope keywords -->
    <rule ref="Squiz.WhiteSpace.ScopeKeywordSpacing"/>
    <!-- There must be no whitespace before a semicolon -->
    <rule ref="Squiz.WhiteSpace.SemicolonSpacing"/>
    <!-- There must be (among others): -->
    <!-- # no whitespaces at the end of line -->
    <!-- # no two consecutive empty lines -->
    <rule ref="Squiz.WhiteSpace.SuperfluousWhitespace"/>

    <!-- to review -->
    <rule ref="Squiz.Functions.MultiLineFunctionDeclaration"/>
    <rule ref="Squiz.Functions.MultiLineFunctionDeclaration.BraceOnSameLine">
        <severity>0</severity>
    </rule>
    <rule ref="Squiz.Functions.MultiLineFunctionDeclaration.SpaceAfterFunction">
        <severity>0</severity>
    </rule>
    <!-- Already covered by: -->
    <!-- Generic.Functions.OpeningFunctionBraceKernighanRitchie.SpaceBeforeBrace -->
    <rule ref="Squiz.Functions.MultiLineFunctionDeclaration.SpaceBeforeOpenBrace">
        <severity>0</severity>
    </rule>
    <rule ref="Squiz.Functions.MultiLineFunctionDeclaration.SpaceAfterUse">
        <severity>0</severity>
    </rule>
    <!-- <rule ref="Squiz.Operators.ComparisonOperatorUsage"/> -->
</ruleset>
